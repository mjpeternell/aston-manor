<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<section id="page-title" class="page-title singular row">
<div class="col-lg-12">
    <h1 class="wow fadeIn funnyText animated" data-wow-delay="1s" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-delay: 1s;">Events</h1>
</div>
</section>
<div class="singular row">
    <div id="primary" class="content-area col-lg-12">
        <div id="content" class="site-content" role="main">

            <?php while (have_posts()) : the_post(); ?>

                <?php //pixelfire_content_nav('nav-above'); ?>

                <?php get_template_part('content', 'single-event'); ?>

                <?php pixelfire_content_nav('nav-below'); ?>

                <?php
                // If comments are open or we have at least one comment, load up the comment template
                if (comments_open() || '0' != get_comments_number())
                    comments_template('', true);
                ?>

            <?php endwhile; // end of the loop. ?>

        </div><!-- #content .site-content -->
    </div><!-- #primary .content-area -->

    <?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>
<?php
/**
 * Template Name: Venue Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<?php $themeLink = get_stylesheet_directory_uri(); ?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        $hero_image = get_field('hero_image');
        if (!empty($hero_image)):
            // vars
            $hero_url = $hero_image['url'];
            $hero_alt = $hero_image['alt'];
            $hero_width = $hero_image['width'];
            $hero_height = $hero_image['height'];

        endif;
        ?>
        <section id="Hero" data-section-name="Hero" class="section hero fp-section fp-table parallax-window" data-position="0px 0px" data-natural-width="<?php echo $hero_width; ?>" data-natural-height="<?php echo $hero_height; ?>" data-image-src="<?php echo $hero_url; ?>" data-speed="0.5" data-bleed="0" data-parallax="scroll" style="height:<?php echo $hero_height . 'px'; ?>">
            <div class="hero-caption white left">
                <?php
                if (get_field('hero_title')) {
                    echo '<h1>' . get_field('hero_title') . '</h1>';
                }
                if (get_field('hero_subtitle')) {
                    echo '<h2>' . get_field('hero_subtitle') . '</h2>';
                }
                if (get_field('hero_cta_buttton_label')) {
                    echo $my_url = "";
                    if (get_field('hero_cta_buttton_link')) {
                        $my_url = get_field('hero_cta_buttton_link');
                    } else {
                        $my_url = '#';
                    }
                    if (get_field('optinmonter_slug')) {
                        $my_optin = get_field('optinmonter_slug');
                    }
                    echo '<div class="btn-group text-left">';
                    echo '<a href=' . $my_url . ' role="button" class="btn btn-black manual-optin-trigger" data-optin-slug="' . $my_optin . '">' . get_field('hero_cta_buttton_label') . '</a>';
                    echo '</div>';
                }
                ?>
            </div>  
        </section>
        <?php
        $hero_image_venue = get_field('hero_image_venue');
        if (!empty($hero_image_venue)):
            // vars
            $hero_url = $hero_image_venue['url'];
            $hero_alt = $hero_image_venue['alt'];
            $hero_width = $hero_image_venue['width'];
            $hero_height = $hero_image_venue['height'];

        endif;
        ?>
        <div class="img-spacer top">&nbsp;</div>

        <?php
// check if the flexible content field has rows of data
        if (have_rows('flex_layouts')):

            // loop through the rows of data
            while (have_rows('flex_layouts')) : the_row();
                $flex_layout++;
                ?>
                <section id="FlexLayout1-<?php echo $flex_layout; ?>" class="section wysiwyg-row">
                    <?php
                    if (get_row_layout() == '12_columns_layout'):
                        echo "<div class=\"col-lg-12\">";
                        the_sub_field('12_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '66_columns_layout'):
                        echo "<div class=\"col-lg-6\">";
                        the_sub_field('6_column_editor_left');
                        echo "</div>";
                        echo "<div class=\"col-lg-6\">";
                        the_sub_field('6_column_editor_right');
                        echo "</div>";
                    elseif (get_row_layout() == '84_columns_layout'):
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '48_columns_layout'):
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                    endif;
                    ?>
                </section>
                <?php
            endwhile;

        else :

        // no layouts found

        endif;
        ?>

        <section id="AboutDivider" class="section full-bleed-row about-divider">
            <div id="ab-left" class="col-lg-7">
                <?php
                if (get_field('fb_left_column')) {
                    echo get_field('fb_left_column', false, false);
                }
                ?>
            </div>
            <div id="ab-right" class="col-lg-5">
                <?php
                if (get_field('fb_right_column')) {
                    echo get_field('fb_right_column', false, false);
                }
                ?>
            </div>
        </section>
        <!-- Flex Layout 2 -->
        <?php
// check if the flexible content field has rows of data
        if (have_rows('flex_layouts_2')):

            // loop through the rows of data
            while (have_rows('flex_layouts_2')) : the_row();
                $flex2_counter++;
                ?>
                <section id="FlexLayout2-<?php echo $flex2_counter; ?>" class="section wysiwyg-row">
                    <?php
                    if (get_row_layout() == '12_columns_layout'):
                        echo "<div class=\"col-lg-12\">";
                        the_sub_field('12_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '66_columns_layout'):
                        echo "<div class=\"col-lg-6\">";
                        the_sub_field('6_column_editor_left');
                        echo "</div>";
                        echo "<div class=\"col-lg-6\">";
                        the_sub_field('6_column_editor_right');
                        echo "</div>";
                    elseif (get_row_layout() == '84_columns_layout'):
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '48_columns_layout'):
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                    endif;
                    ?>
                </section>
                <?php
            endwhile;

        else :

        // no layouts found

        endif;
        ?>

        <section id="tabDivider" class="section full-bleed-row about-divider">
            <div class="tabDivider-row">
                <div id="ab-left" class="col-lg-6">
                    <ul class="accordion-tabs-minimal">
                        <?php if (get_field('tab_1_label')) { ?>
                            <?php echo '<li class="tab-header-and-content">'; ?>
                            <a href="#" class="tab-link is-active"><?php echo get_field('tab_1_label', false, false); ?></a>
                            <div class="tab-content entry-content">
                                <?php
                                if (get_field('tab_1_content')) {
                                    echo get_field('tab_1_content', false, false);
                                }
                                ?>
                                <?php if (get_field('tab_1_btn_url')) { ?>
                                    <div class="btn-group">
                                        <a class="btn btn-black " href="<?php echo get_field('tab_1_btn_url'); ?>" target="_blank"><?php echo get_field('tab_1_btn_label'); ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php echo '</li>'; ?>
                        <?php } ?>
                        <?php if (get_field('tab_2_label')) { ?>
                            <?php echo '<li class="tab-header-and-content">'; ?>
                            <a href="#" class="tab-link"><?php echo get_field('tab_2_label', false, false); ?></a>
                            <div class="tab-content entry-content">
                                <?php
                                if (get_field('tab_2_content')) {
                                    echo get_field('tab_2_content', false, false);
                                }
                                ?>

                                <?php if (get_field('tab_2_btn_url')) { ?>
                                    <div class="btn-group">
                                        <a class="btn btn-black " href="<?php echo get_field('tab_2_btn_url'); ?>" target="_blank"><?php echo get_field('tab_2_btn_label'); ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php echo '</li>'; ?>
                        <?php } ?>
                    </ul>    
                </div>
                <div id="ab-right" class="col-lg-6">
                    <div class="img-row">
                        <?php
                        $v_image_1 = get_field('venue_image_up_left');
                        if (!empty($v_image_1)):
                            ?>
                            <div class="img-col-6"> <img class="img-resp" src="<?php echo $v_image_1['sizes']['instagram-square']; ?>" alt="<?php echo $v_image_1['alt']; ?>" /></div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </section>
        <div class="img-spacer top">
            <img src="<?php echo$themeLink; ?>/assets/images/am-venue-art-deco-trim_03.png" class="img-responsive"/>
        </div>
        <section id="Calander" class="section calendar non-parallax-window">
            <header class="section-header text-center">
                <h2>The Aston Mob</h2>
                <h32>Resident & Guest DJ’s</h3>
            </header>
            <div class="bxslider">
                <?php
                $argsx = array(
                    'post_type' => 'dj-profiles',
                    'posts_per_page' => 12,
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'order' => 'ASC',
                );
                $future_posts = get_posts($argsx);
                foreach ($future_posts as $kk) {
                    $thumb_size = 'instagram-square';
                    $img_id = get_post_thumbnail_id($kk->ID); // This gets just the ID of the img
                    $image = wp_get_attachment_image_src($img_id, $thumb_size); // Get URL of the image, and size can be set here too (same as with get_the_post_thumbnail, I think)
                    $alt_text = get_post_meta($img_id, '_wp_attachment_image_alt', true);
                    $perm = get_permalink($kk->ID);
                    ?>
                    <div class="slide">
                        <div class="image_holder">
                            <span class="image">
                                <span class="image_pixel_hover"></span>
                                <a  target="_self" href="<?php echo $perm; ?>"><img src="<?php echo $image[0]; ?>" class="slide-image" alt="<?php echo $alt_text; ?>" /></a>
                            </span>
                            <div class="hover_feature_holder">
                                <div class="hover_feature_holder_outer">
                                    <div class="hover_feature_holder_inner">
                                        <h3 class="portfolio_title"><?php echo $kk->post_title; ?></h3>
                                        <span class="separator small"></span>

                                        <div class="project_category">
                                            <div class="tags">
                                                <?php
                                                foreach ((get_the_category($kk->ID)) as $category) {
                                                    echo "<span>" . $category->cat_name . "</span>";
                                                    //print_r($category);
                                                }
                                                ?>
                                            </div>
                                            <div class="tags">
                                                <?php
                                                $posttags = get_the_tags($kk->ID);
                                                if ($posttags) {
                                                    foreach ($posttags as $tag) {
                                                        echo "<span>" . $tag->name . "</span>";
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <a class="lightbox qbutton white small" href="<?php echo $image[0]; ?>" title="<?php echo $alt_text; ?>" rel="prettyPhoto">zoom</a>
                                        <a class="qbutton white small" target="_self" href="<?php echo $perm; ?>">view</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                <?php } ?>
                <?php wp_reset_query(); ?>
            </div>
        </section>
        <div class="img-spacer bottom"><img src="<?php echo$themeLink; ?>/assets/images/am-venue-art-deco-trim_06.png" class="img-responsive"/></div>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_footer(); ?>
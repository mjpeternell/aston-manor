<?php
/**
 * Template Name: Home Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<?php $themeLink = get_stylesheet_directory_uri(); ?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        $hero_image = get_field('hero_image');
        if (!empty($hero_image)):
            // vars
            $hero_url = $hero_image['url'];
            $hero_alt = $hero_image['alt'];
            $hero_width = $hero_image['width'];
            $hero_height = $hero_image['height'];

        endif;
        ?>
        <section id="Hero" data-section-name="Hero" class="section hero fp-section fp-table parallax-window" data-position="0px -200px" data-natural-width="<?php echo $hero_width; ?>" data-natural-height="<?php echo $hero_height; ?>" data-image-src="<?php echo $hero_url; ?>" data-speed="0.5" data-bleed="0" data-parallax="scroll" style="height:<?php echo $hero_height; ?>px">
            <div class="hero-caption white left">
                <?php
                if (get_field('hero_title')) {
                    echo '<h1>' . get_field('hero_title') . '</h1>';
                }
                if (get_field('hero_subtitle')) {
                    echo '<h2>' . get_field('hero_subtitle') . '</h2>';
                }
                if (get_field('hero_cta_buttton_label')) {
                    echo $my_url = "";
                    if (get_field('hero_cta_buttton_link')) {
                        $my_url = get_field('hero_cta_buttton_link');
                    } else {
                        $my_url = '#';
                    }
                    if (get_field('optinmonter_slug')) {
                        $my_optin = get_field('optinmonter_slug');
                    }
                    echo '<div class="btn-group text-left">';
                    echo '<a href=' . $my_url . ' role="button" class="btn btn-black manual-optin-trigger" data-optin-slug="'.$my_optin.'">' . get_field('hero_cta_buttton_label') . '</a>';
                    //echo '<a href='.$my_url.' role="button" class="btn-secondary">'.get_field('hero_cta_buttton_label').'</a>';
                    echo '</div>';
                }
                ?>
            </div>  
        </section>
        <section id="Calander" class="section calendar non-parallax-window">
            <header class="section-header text-center">
                <h2>Coming Soon to Aston</h2>
                <h3>Check Out Some of Our Upcoming Events</h3>
            </header>
            <?php the_content(); ?>    
        </section>
        <section id="Aston-Video" class="section video-section non-parallax-window">
        <?php 
        $themeLink = get_stylesheet_directory_uri(); 
        $video_1 = "Aston-Manor-Home-360p";
        ?>  
           
            <?php if (get_field('home_page_text_over_video')) { ?>
                <div class="text-layer entry-content">
                    <?php echo get_field('home_page_text_over_video'); ?>
                </div>
            <?php } ?>
            <div class="embed-container"
            data-vide-bg="mp4:<?php echo $themeLink; ?>/assets/video/<?php echo $video_1; ?>.mp4 ,poster: <?php echo $themeLink; ?>/assets/video/<?php echo $video_1; ?>.jpg"
            data-vide-options="posterType: jpg, loop: true"     
            ></div>
            <?php
            $rgba = "";
            $opacity_level = "";
            $opacity_color = "";
            $alpha = "";
            if (get_field('video_overlay_opacity')) {
                $opacity_level = get_field('video_overlay_opacity');
            } else {
                $opacity_level = '0.25';
            }
            if (get_field('video_overlay_color')) {
                $opacity_color = get_field('video_overlay_color');
                list($r, $g, $b) = array_map('hexdec', str_split(ltrim($opacity_color, '#'), 2));
                $opacity_color = $r . ',' . $g . ',' . $b;
            } else {
                $opacity_color = '0,0,0';
            }
            ?>
             <div class="opacity-layer" style="background-color: <?php echo 'rgba(' . $opacity_color . ',' . $opacity_level . ')'; ?>"></div>
        </section>

        <section id="PastEvents" class="past-events section non-parallax-window">
            <header class="section-header text-center">
                <div class="col-lg-12">
                    <?php
                    if (get_field('home_page_gallery_title')) {
                        echo '<h2 class="mobile-no">' . get_field('home_page_gallery_title') . '</h2>';
                    }
                    if (get_field('home_page_gallery_subtitle')) {
                        echo '<h3>' . get_field('home_page_gallery_subtitle') . '</h3>';
                    }
                    ?>
                </div>
            </header>
            <?php if (get_field('home_page_photo_gallery')) { ?>
                <div class="wysiwyg-row">
                    <div class="col-lg-12">
                        <?php echo get_field('home_page_photo_gallery'); ?>
                    </div>
                </div>
            <?php } ?>
        </section>
        <?php
        $image = get_field('about_background_image');

        if (!empty($image)):
            // vars
            $url = $image['url'];
            $width = $image['width'];
            $height = $image['height'];

        endif;
        ?>
        <section id="About" class="section hero fp-section fp-table parallax-window" data-natural-width="<?php echo $width; ?>" data-natural-height="<?php echo $height; ?>" data-image-src="<?php echo $url; ?>" data-speed="0.5" data-bleed="0" data-parallax="scroll" data-position="0px 0px" style="height:<?php echo $height - 50; ?>">
            <div class="about-caption white right">
                <?php
                if (get_field('about_copy')) {
                    echo get_field('about_copy');
                }
                ?>
                <div class="row am-key-logo"><img src="<?php echo $themeLink; ?>/assets/images/am-logo_06.png" class="img-responsive"/></div>
            </div>
        </section>

        <?php
// check if the flexible content field has rows of data
        if (have_rows('flex_layouts')):
            $flex_counter = 0;
            // loop through the rows of data
            while (have_rows('flex_layouts')) : the_row();
                $flex_counter++;
                ?>
                <section id="ContentBox-<?php echo $flex_counter; ?>" class="flex-layout section">
                    <div class="full-bleed-row home-contact">
                        <?php
                        if (get_row_layout() == '12_columns_layout'):
                            echo "<div class=\"col-lg-12\">";
                            the_sub_field('12_column_editor',false, false);
                            echo "</div>";
                        elseif (get_row_layout() == '66_columns_layout'):
                            echo "<div class=\"col-lg-6\">";
                            the_sub_field('6_column_editor_left',false, false);
                            echo "</div>";
                            echo "<div class=\"col-lg-6\">";
                            the_sub_field('6_column_editor_right',false, false);
                            echo "</div>";
                        elseif (get_row_layout() == '84_columns_layout'):
                            echo "<div class=\"col-lg-8\">";
                            the_sub_field('8_column_editor',false, false);
                            echo "</div>";
                            echo "<div class=\"col-lg-4\">";
                            the_sub_field('4_column_editor',false, false);
                            echo "</div>";
                        elseif (get_row_layout() == '48_columns_layout'):
                            echo "<div class=\"col-lg-4\">";
                            the_sub_field('4_column_editor',false, false);
                            echo "</div>";
                            echo "<div class=\"col-lg-8\">";
                            the_sub_field('8_column_editor',false, false);
                            echo "</div>";
                        endif;
                        ?>
                    </div>
                </section>
                <?php
            endwhile;

        else :

        // no layouts found

        endif;
        ?>
    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();         ?>
<?php get_footer(); ?>
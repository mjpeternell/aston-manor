<?php
/**
 * Template Name: Gallery Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<?php $themeLink = get_stylesheet_directory_uri(); ?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <section id="Aston-Video" class="section video-section non-parallax-window">
            <?php
            $themeLink = get_stylesheet_directory_uri();
            $video_1 = "Aston-Gallery-Page";
            ?>

            <div id="Gallery-Hero-Vid" class="embed-container gallery-pg"
                 data-vide-bg="mp4:<?php echo $themeLink; ?>/assets/video/<?php echo $video_1; ?>.mp4 ,poster: <?php echo $themeLink; ?>/assets/video/<?php echo $video_1; ?>.jpg"
                 data-vide-options="posterType: jpg, loop: true"     
                 ></div>
                 <?php
                 $rgba = "";
                 $opacity_level = "";
                 $opacity_color = "";
                 $alpha = "";
                 if (get_field('video_overlay_opacity')) {
                     $opacity_level = get_field('video_overlay_opacity');
                 } else {
                     $opacity_level = '0.25';
                 }
                 if (get_field('video_overlay_color')) {
                     $opacity_color = get_field('video_overlay_color');
                     list($r, $g, $b) = array_map('hexdec', str_split(ltrim($opacity_color, '#'), 2));
                     $opacity_color = $r . ',' . $g . ',' . $b;
                 } else {
                     $opacity_color = '0,0,0';
                 }
                 ?>
            <div class="opacity-layer <?php
            if ($opacity_level == 0) {
                echo 'hide';
            }
            ?>" style="background-color: <?php echo 'rgba(' . $opacity_color . ',' . $opacity_level . ')'; ?>"></div>
            <?php if (get_field('gallery_title')) { ?>
                <div class="hero-caption white left">
                    <?php echo '<h1>' . get_field('gallery_title') . '</h1>'; ?>
                <?php } ?>
                <?php
                if (get_field('gallery_subtitle')) {
                    echo '<h2>' . get_field('gallery_subtitle') . '</h2>';
                    ?>
                </div>
            <?php } ?>

        </section>
        <section id="PastEvents" class="past-events section non-parallax-window">
            <header class="section-header text-center">
                <div class="col-lg-12">
                    <?php
                    if (get_field('home_page_gallery_title')) {
                        echo '<h1>' . get_field('home_page_gallery_title') . '</h1>';
                    }
                    if (get_field('home_page_gallery_subtitle')) {
                        echo '<h2>' . get_field('home_page_gallery_subtitle') . '</h2>';
                    }
                    ?>
                </div>
            </header>
            <?php if (get_field('home_page_photo_gallery')) { ?>
                <div class="wysiwyg-row">
                    <div class="col-lg-8">
                        <?php echo get_field('home_page_photo_gallery'); ?>
                    </div>
                </div>
            <?php } ?>
        </section>
        <header class="section-header text-center">
            <h2>PHOTOS FROM PAST EVENTS</h2>
            <h3>CHECK OUT SOME OF OUR RECENT EVENTS BELOW</h3>
        </header>
        <?php
// check if the flexible content field has rows of data
        if (have_rows('flex_layouts')):

            // loop through the rows of data
            while (have_rows('flex_layouts')) : the_row();
                $flex_layout_count++;
                ?>
                <section id="flex-layout-<?php echo $flex_layout_count; ?>" class="section wysiwyg-row">
                    <?php
                    if (get_row_layout() == '12_columns_layout'):
                        echo "<div class=\"col-lg-12\">";
                        the_sub_field('12_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '84_columns_layout'):
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '48_columns_layout'):
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                    endif;
                    ?>
                </section>
                <?php
            endwhile;

        else :

        // no layouts found

        endif;
        ?>
        <section id="galleryVideo" class="section full-bleed-row gallery-slider">
            <div id="gallery-vid-left" class="col-lg-8">
                <?php
                if (get_field('video_slider')) {
                    echo '<p>' . get_field('video_slider') . '</p>';
                }
                ?>
            </div>
            <div id="gallery-vid-right" class="col-lg-4">
                <?php
                if (get_field('video_slider_text')) {
                    echo '<p>' . get_field('video_slider_text') . '</p>';
                }
                ?>
            </div>
        </section>
        <!-- Flex Layout 2 -->
        <?php
// check if the flexible content field has rows of data
        if (have_rows('flex_layouts_2')):

            // loop through the rows of data
            while (have_rows('flex_layouts_2')) : the_row();
                $flex_layout2_count++;
                ?>
                <section id="flex-layout2-<?php echo $flex_layout2_count; ?>" class="section wysiwyg-row">
                    <?php
                    if (get_row_layout() == '12_columns_layout'):
                        echo "<div class=\"col-lg-12\">";
                        the_sub_field('12_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '84_columns_layout'):
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '48_columns_layout'):
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                    endif;
                    ?>
                </section>
                <?php
            endwhile;

        else :

        // no layouts found

        endif;
        ?>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();        ?>
<?php get_footer(); ?>
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
?>

</div><!-- #main .site-main -->


</div><!-- #page .hfeed .site -->
<footer id="footer-area" class="footer" role="contentinfo">
    <div class="footer-links row">
        <?php if (is_active_sidebar('footer-widget-1')) : ?>
            <div class="col-lg-3 footer-widget" role="complementary">
                <?php dynamic_sidebar('footer-widget-1'); ?>
            </div><!-- .widget-area .first -->
        <?php endif; ?>

        <?php if (is_active_sidebar('footer-widget-2')) : ?>
            <div class="col-lg-3 footer-widget" role="complementary">
                <?php dynamic_sidebar('footer-widget-2'); ?>
            </div><!-- .widget-area .second -->
        <?php endif; ?>

        <?php if (is_active_sidebar('footer-widget-3')) : ?>
            <div class="col-lg-3 footer-widget" role="complementary">
                <?php dynamic_sidebar('footer-widget-3'); ?>
            </div><!-- .widget-area .third -->
        <?php endif; ?>
        <?php if (is_active_sidebar('footer-widget-4')) : ?>
            <div class="col-lg-3 footer-widget" role="complementary">
                <?php dynamic_sidebar('footer-widget-4'); ?>
            </div><!-- .widget-area .third -->
        <?php endif; ?>
    </div>
    <div class="site-info copyright"  role="contentinfo">
        <?php echo of_get_option('custom_footer_text', 'pixelfire'); ?>
    </div><!-- .site-info -->
</footer>

<div class="scroll-to-top" style="display: block;">
    <i class="fa fa-angle-up"></i>
</div>
<?php wp_footer(); ?>
</body>
</html>
<?php

/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * @since WP PixelFire Theme 1.0
 */
function pixelfire_page_menu_args($args) {
    $args['show_home'] = true;
    return $args;
}

add_filter('wp_page_menu_args', 'pixelfire_page_menu_args');

/**
 * Adds custom classes to the array of body classes.
 *
 * @since WP PixelFire Theme 1.0
 */
function pixelfire_body_classes($classes) {
    // Adds a class of group-blog to blogs with more than 1 published author
    if (is_multi_author()) {
        $classes[] = 'group-blog';
    }

    return $classes;
}

add_filter('body_class', 'pixelfire_body_classes');

/**
 * Filter in a link to a content ID attribute for the next/previous image links on image attachment pages
 *
 * @since WP PixelFire Theme 1.0
 */
function pixelfire_enhanced_image_navigation($url, $id) {
    if (!is_attachment() && !wp_attachment_is_image($id))
        return $url;

    $image = get_post($id);
    if (!empty($image->post_parent) && $image->post_parent != $id)
        $url .= '#main';

    return $url;
}

add_filter('attachment_link', 'pixelfire_enhanced_image_navigation', 10, 2);

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @since WP PixelFire Theme 1.1
 */
function pixelfire_wp_title($title, $sep) {
    global $page, $paged;

    if (is_feed())
        return $title;

    // Add the blog name
    $title .= get_bloginfo('name');

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && ( is_home() || is_front_page() ))
        $title .= " $sep $site_description";

    // Add a page number if necessary:
    if ($paged >= 2 || $page >= 2)
        $title .= " $sep " . sprintf(__('Page %s', 'pixelfire'), max($paged, $page));

    return $title;
}

add_filter('wp_title', 'pixelfire_wp_title', 10, 2);

/**
 * Add custom favicon displayed in WordPress dashboard and frontend
 */
function pixelfire_add_favicon() {
    if (of_get_option('custom_favicon')) {
        echo '<link rel="shortcut icon" type="image/x-icon" href="' . of_get_option('custom_favicon') . '" />' . "\n";
    }
}

add_action('wp_head', 'pixelfire_add_favicon', 0);
add_action('admin_head', 'pixelfire_add_favicon', 0);


/**
 * Add custom Social Icons displayed on frontend and set in WordPress dashboard.
 */
if (!function_exists('sparkling_social')) :

    /**
     * Display social links in footer and widgets if enabled
     */
    function sparkling_social() {
        $services = array(
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'googleplus' => 'Google+',
            'youtube' => 'YouTube',
            'vimeo' => 'Vimeo',
            'linkedin' => 'LinkedIn',
            'pinterest' => 'Pinterest',
            'rss' => 'RSS',
            'tumblr' => 'Tumblr',
            'flickr' => 'Flickr',
            'instagram' => 'Instagram',
            'dribbble' => 'Dribbble',
            'skype' => 'Skype',
            'foursquare' => 'Foursquare',
            'soundcloud' => 'SoundCloud',
            'github' => 'GitHub'
        );

        echo '<div class="social-icons">';

        foreach ($services as $service => $name) :

            $active[$service] = of_get_option('social_' . $service);
            if ($active[$service]) {
                echo '<a href="' . esc_url($active[$service]) . '" title="' . __('Follow us on ', 'sparkling') . $name . '" class="' . $service . '" target="_blank"><i class="social_icon fa fa-' . $service . '"></i></a>';
            }

        endforeach;
        echo '</div>';
    }

endif;





if (!function_exists('pixelfire_theme_options')) {

    /**
     * Get information from Theme Options and add it into wp_head
     */
    function pixelfire_theme_options() {

        echo '<style type="text/css">';
        //Body

        $background = of_get_option('body_bg_image');
        if ($background) {
           
                echo 'body {';
                if ($background['color']) {
                    echo 'background-color: ' . $background['color'] . '; ';
                }
                 if ($background['image']) {
                    if ($background['image']) {
                        echo 'background-image: url(' . $background['image'] . '); ';
                    }
                    if ($background['repeat']) {
                        echo 'background-repeat: ' . $background['repeat'] . ';';
                    }
                    if ($background['position']) {
                        echo 'background-position: ' . $background['position'] . '; ';
                    }
                    if ($background['attachment']) {
                        echo 'background-attachment: ' . $background['attachment'] . '; ';
                    }
                }
                  echo '}';
        }
        
        
        $button = of_get_option('button_styling');
        if ($button) {
            echo 'a.btn-primary[role="button"] {'
            . 'font-family: "' . $button['face'] . '", sans-serif; '
            . 'font-size:' . $button['size'] . ';'
            . 'font-style: ' . $button['style'] . '; '
            . 'font-weight: ' . $button['weight'] . '; '
            . 'color:' . $button['color'] . ';'
            . 'border-radius:' . $button['radius'] . ';'
            . 'background-color: ' . of_get_option('primary_btn_bg_color') . ';'
            . '}';
        }

        if ($button) {
            echo 'a.btn-primary[role="button"]:hover {'
            . 'color:' . $button['color'] . ';'
            . 'background-color: ' . of_get_option('primary_btn_bg_color_hover') . ';'
            . '}';
        }

        $button1 = of_get_option('secondary_button_styling');
        if ($button1) {
            echo 'a.btn-secondary[role="button"] {'
            . 'font-family: "' . $button1['face'] . '", sans-serif; '
            . 'font-size:' . $button1['size'] . ';'
            . 'font-style: ' . $button1['style'] . '; '
            . 'font-weight: ' . $button1['weight'] . '; '
            . 'color:' . $button1['color'] . ';'
            . 'border-radius:' . $button1['radius'] . ';'
            . 'background-color: ' . of_get_option('secondary_btn_bg_color') . ';'
            . '}';
        }

//        if (of_get_option('secondary_btn_bg_color')) {
//            echo 'a.btn-secondary[role="button"] { '
//            . 'background-color: ' . of_get_option('secondary_btn_bg_color') . '; } ';
//        }
//        
        if (of_get_option('secondary_btn_bg_color_hover')) {
            echo 'a.btn-secondary[role="button"]:hover { '
            . 'background-color: ' . of_get_option('secondary_btn_bg_color_hover') . '; } ';
        }

        if (of_get_option('link_color')) {
            echo 'a, #infinite-handle span, #secondary .widget .post-content a {color:' . of_get_option('link_color') . '}';
        }
        if (of_get_option('link_hover_color')) {
            echo 'a:hover, a:active, #secondary .widget .post-content a:hover {color: ' . of_get_option('link_hover_color') . ';}';
        }
        if (of_get_option('anchor_element_color')) {
            echo 'a:hover, .centered-navigation ul li a {color: ' . of_get_option('anchor_element_color') . '; border-color: ' . of_get_option('anchor_element_color') . ';} .site-main [class*="navigation"] a, .more-link { color: ' . of_get_option('anchor_element_color') . '}';
        }
        if (of_get_option('anchor_element_color_hover')) {
            echo 'a:hover, .centered-navigation ul li a:focus, '
            . '.centered-navigation ul li a:hover,'
            . '.site-main [class*="navigation"] a:hover, '
            . '.more-link a:hover'
            . ' { '
            . 'color: ' . of_get_option('anchor_element_color_hover') . '; '
            . '}';
        }

        if (of_get_option('cfa_color')) {
            echo 'a.btn-primary[role="button"], .ninja-forms-field[type="submit"]  { color: ' . of_get_option('cfa_color') . ';}';
        }
        if (of_get_option('cfa_btn_color') || of_get_option('cfa_btn_txt_color')) {
            echo '.cfa-button {border-color: ' . of_get_option('cfa_btn_color') . '; color: ' . of_get_option('cfa_btn_txt_color') . ';}';
        }
        if (of_get_option('heading_color')) {
            echo 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .entry-title {color: ' . of_get_option('heading_color') . ';}';
        }
        if (of_get_option('nav_bg_color')) {
            echo '.navbar.navbar-default {background-color: ' . of_get_option('nav_bg_color') . ';}';
        }
        if (of_get_option('nav_link_color')) {
            echo '.navbar-default .navbar-nav > li > a, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus { color: ' . of_get_option('nav_link_color') . ';}';
        }
        if (of_get_option('nav_item_hover_color')) {
            echo '.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus, .entry-title a:hover {color: ' . of_get_option('nav_item_hover_color') . ';}';
        }
        if (of_get_option('nav_dropdown_bg')) {
            echo '.dropdown-menu {background-color: ' . of_get_option('nav_dropdown_bg') . ';}';
        }
        if (of_get_option('nav_dropdown_item')) {
            echo '.navbar-default .navbar-nav .open .dropdown-menu > li > a, .dropdown-menu > li > a { color: ' . of_get_option('nav_dropdown_item') . ';}';
        }
        if (of_get_option('nav_dropdown_bg_hover') || of_get_option('nav_dropdown_item_hover')) {
            echo '.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus, .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus {background-color: ' . of_get_option('nav_dropdown_bg_hover') . '; color:' . of_get_option('nav_dropdown_item_hover') . '}';
        }


        //Footer
        if (of_get_option('footer_bg_image')) {
            echo '#footer-area {background: url(' . of_get_option('footer_bg_image') . ') repeat-x;}';
        }
        if (of_get_option('footer_bg_color')) {
            echo '#footer-area {background-color: ' . of_get_option('footer_bg_color') . ';}';
        }
        if (of_get_option('footer_text_color')) {
            echo '#footer-area, .site-info, address {color: ' . of_get_option('footer_text_color') . ';}';
        }
        if (of_get_option('footer_widget_bg_color')) {
            echo '.footer-widget {background-color: ' . of_get_option('footer_widget_bg_color') . ';}';
        }

        //Site Info
        if (of_get_option('footer_link_color')) {
            echo '.site-info a, #footer-area a {color: ' . of_get_option('footer_link_color') . ';}';
        }
        if (of_get_option('footer_link_hover_color')) {
            echo '.site-info a:hover, #footer-area a:hover {color: ' . of_get_option('footer_link_hover_color') . '!important;}';
        }
        if (of_get_option('social_color')) {
            echo '.social-icons a {background-color: ' . of_get_option('social_color') . ' !important;}';
        }
        if (of_get_option('social_link_color')) {
            echo '.social-icons a {color: ' . of_get_option('social_link_color') . ' !important;}';
        }

        if (of_get_option('social_link_hover_color')) {
            echo '.social-icons a:hover {color: ' . of_get_option('social_link_hover_color') . ' !important;}';
        }

        if (of_get_option('social_footer_color')) {
            echo '#footer-area .social-icons a {background-color: ' . of_get_option('social_footer_color') . ' ;}';
        }


        $typography = of_get_option('main_body_typography');
        if ($typography) {
            echo 'body, .entry-content {'
            . 'font-family: ' . $typography['face'] . ' , sans-serif; '
            . 'font-size:' . $typography['size'] . ';'
            . 'font-style: ' . $typography['style'] . '; '
            . 'font-weight: ' . $typography['weight'] . '; ';
            //. 'color:' . $typography['color'] . ';}';
        }

        $typography = of_get_option('h1_heading_typography');
        if ($typography) {
            echo 'h1, .h1 {';
            echo "font-family: '" . $typography['face'] . "', arial, sans-serif;"
            . 'font-size:' . $typography['size'] . ';'
            . 'font-style: ' . $typography['style'] . '; '
            . 'font-weight: ' . $typography['weight'] . '; '
            . 'color:' . $typography['color'] . ';}';
        }

        $typography = of_get_option('h2_heading_typography');
        if ($typography) {
            echo 'h2, .h2 {'
            . 'font-family: ' . $typography['face'] . ', sans-serif; '
            . 'font-size:' . $typography['size'] . ';'
            . 'font-style: ' . $typography['style'] . '; '
            . 'font-weight: ' . $typography['weight'] . '; '
            . 'color:' . $typography['color'] . ';}';
        }

        $typography = of_get_option('h3_heading_typography');
        if ($typography) {
            echo 'h3, .h3 {'
            . 'font-family: ' . $typography['face'] . ', sans-serif; '
            . 'font-size:' . $typography['size'] . ';'
            . 'font-style: ' . $typography['style'] . '; '
            . 'font-weight: ' . $typography['weight'] . '; '
            . 'color:' . $typography['color'] . ';}';
        }

        $typography = of_get_option('h4_heading_typography');
        if ($typography) {
            echo 'h4, .h4 {'
            . 'font-family: ' . $typography['face'] . ', sans-serif; '
            . 'font-size:' . $typography['size'] . ';'
            . 'font-style: ' . $typography['style'] . '; '
            . 'font-weight: ' . $typography['weight'] . '; '
            . 'color:' . $typography['color'] . ';}';
        }

        $typography = of_get_option('h5_heading_typography');
        if ($typography) {
            echo 'h5, .h5 {'
            . 'font-family: ' . $typography['face'] . ', sans-serif; '
            . 'font-size:' . $typography['size'] . ';'
            . 'font-style: ' . $typography['style'] . '; '
            . 'font-weight: ' . $typography['weight'] . '; '
            . 'color:' . $typography['color'] . ';}';
        }

        $typography = of_get_option('h6_heading_typography');
        if ($typography) {
            echo 'h6, .h6 {'
            . 'font-family: ' . $typography['face'] . ', sans-serif; '
            . 'font-size:' . $typography['size'] . ';'
            . 'font-style: ' . $typography['style'] . '; '
            . 'font-weight: ' . $typography['weight'] . '; '
            . 'color:' . $typography['color'] . ';}';
        }

        if (of_get_option('custom_css')) {
            echo of_get_option('custom_css', 'no entry');
        }
        echo '</style>';
    }

}
add_action('wp_head', 'pixelfire_theme_options', 10);


/**
 * Header menu (should you choose to use one)
 */
if (!function_exists('pixelfire_header_menu()')) :

    function pixelfire_header_menu() {
        // display the WordPress Custom Menu if available

        wp_nav_menu(array(
            'menu' => 'mobile',
            'theme_location' => 'mobile',
            'menu_id' => 'js-navigation-menu',
            'depth' => 2,
            'container' => null,
            'container_class' => null,
            'menu_class' => 'nav-wrapper',
            'items_wrap' => '<nav class="%2$s" role="navigation" ><ul id="%1$s" class="navigation-menu show">%3$s</ul></nav>',
                //'fallback_cb'       => 'wp_page_menu',
                //'walker'            => new wp_bootstrap_navwalker()
        ));
    }

/* end header menu */
endif;

if (!function_exists('pixelfire_header_menu()')) :

    function pixelfire_primary_menu() {
        // display the WordPress Custom Menu if available

        wp_nav_menu(array(
            'menu' => 'primary',
            'theme_location' => 'primary',
            'menu_id' => 'js-centered-navigation-menu',
            'depth' => 2,
            'container' => null,
            'container_class' => null,
            'menu_class' => 'nav-wrapper',
            'items_wrap' => '<nav class="%2$s" role="navigation" ><ul id="%1$s" class="centered-navigation-menu show">%3$s</ul></nav>',
                //'fallback_cb'       => 'wp_page_menu',
                //'walker'            => new wp_bootstrap_navwalker()
        ));
    }

/* end header menu */
endif;

if (!function_exists('sparkling_footer_links')) :

    /**
     * Footer menu (should you choose to use one)
     */
    function sparkling_footer_links() {
        // display the WordPress Custom Menu if available
        wp_nav_menu(array(
            'container' => '', // remove nav container
            'container_class' => 'footer-links clearfix', // class of container (should you choose to use it)
            'menu' => __('Footer Links', 'sparkling'), // nav name
            'menu_class' => 'nav footer-nav clearfix', // adding custom nav class
            'theme_location' => 'footer-links', // where it's located in the theme
            'before' => '', // before the menu
            'after' => '', // after the menu
            'link_before' => '', // before each link
            'link_after' => '', // after each link
            'depth' => 0, // limit the depth of the nav
            'fallback_cb' => 'sparkling_footer_links_fallback'  // fallback function
        ));
    }

/* end sparkling footer link */
endif;

/**
 * function to show the footer info, copyright information
 */
function sparkling_footer_info() {
    global $sparkling_footer_info;
    printf(__('Theme by %1$s Powered by %2$s', 'sparkling'), '<a href="http://colorlib.com/" target="_blank">Colorlib</a>', '<a href="http://wordpress.org/" target="_blank">WordPress</a>');
}

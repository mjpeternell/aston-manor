<div class="row text-center">
    <?php if (have_posts()) : ?>

        <?php //pixelfire_content_nav('nav-above');  ?>

        <?php /* Start the Loop */ ?>
        <?php while (have_posts()) : the_post(); ?>
            <div class="col-lg-4">
                <?php
                /* Include the Post-Format-specific template for the content.
                 * If you want to overload this in a child theme then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
                get_template_part('content', get_post_format());
                ?>
            </div>        
        <?php endwhile; ?>

        <?php //pixelfire_content_nav('nav-below');  ?>

    <?php else : ?>

        <?php get_template_part('no-results', 'index'); ?>

    <?php endif; ?>
</div>  
<?php
/**
 * Template Name: Calendar Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<?php $themeLink = get_stylesheet_directory_uri(); ?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        $hero_image = get_field('hero_image');
        if (!empty($hero_image)):
            // vars
            $hero_url = $hero_image['url'];
            $hero_alt = $hero_image['alt'];
            $hero_width = $hero_image['width'];
            $hero_height = $hero_image['height'];

        endif;
        ?>
        <section id="Hero" data-section-name="Hero" class="section hero fp-section fp-table parallax-window" data-position="0px 0px" data-natural-width="<?php echo $hero_width; ?>" data-natural-height="<?php echo $hero_height; ?>" data-image-src="<?php echo $hero_url; ?>" data-speed="0.5" data-bleed="0" data-parallax="scroll" style="height:<?php echo $hero_height.'px'; ?>">
            <div class="hero-caption white left">
                <?php
                if (get_field('hero_title')) {
                    echo '<h1>' . get_field('hero_title') . '</h1>';
                }
                if (get_field('hero_subtitle')) {
                    echo '<h2>' . get_field('hero_subtitle') . '</h2>';
                }
                if (get_field('hero_cta_buttton_label')) {
                    echo $my_url = "";
                    if (get_field('hero_cta_buttton_link')) {
                        $my_url = get_field('hero_cta_buttton_link');
                    } else {
                        $my_url = '#';
                    }
                    echo '<div class="btn-group text-left">';
                    echo '<a href=' . $my_url . ' role="button" class="btn-primary">' . get_field('hero_cta_buttton_label') . '</a>';
                    //echo '<a href='.$my_url.' role="button" class="btn-secondary">'.get_field('hero_cta_buttton_label').'</a>';
                    echo '</div>';
                }
                ?>
            </div>  
        </section>
<!--
        <section id="Calander" class="section calendar non-parallax-window">
            <div class="masonary-tiles-header">
                <h2>Upcoming Events</h2>
            </div>
        </section>-->
        <?php
        $image = get_field('about_background_image');

        if (!empty($image)):
            // vars
            $url = $image['url'];
            $width = $image['width'];
            $height = $image['height'];

        endif;
        ?>
        <?php
// check if the flexible content field has rows of data
        if (have_rows('flex_layouts')):

            // loop through the rows of data
            while (have_rows('flex_layouts')) : the_row();
                $flex_counter++;
                ?>
                <section id="ContentBox-<?php echo $counter; ?>" class="section wysiwyg-row">
                    <?php
                    if (get_row_layout() == '12_columns_layout'):
                        echo "<div class=\"col-lg-12\">";
                        the_sub_field('12_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '66_columns_layout'):
                        echo "<div class=\"col-lg-6\">";
                        the_sub_field('6_column_editor_left');
                        echo "</div>";
                        echo "<div class=\"col-lg-6\">";
                        the_sub_field('6_column_editor_right');
                        echo "</div>";
                    elseif (get_row_layout() == '84_columns_layout'):
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                    elseif (get_row_layout() == '48_columns_layout'):
                        echo "<div class=\"col-lg-4\">";
                        the_sub_field('4_column_editor');
                        echo "</div>";
                        echo "<div class=\"col-lg-8\">";
                        the_sub_field('8_column_editor');
                        echo "</div>";
                    endif;
                    ?>
                </section>
                <?php
            endwhile;

        else :

        // no layouts found

        endif;
        ?>
    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();   ?>
<?php get_footer(); ?>
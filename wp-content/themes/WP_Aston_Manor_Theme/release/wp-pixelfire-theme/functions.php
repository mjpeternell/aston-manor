<?php
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
remove_filter ('acf_the_content', 'wpautop');

add_image_size('instagram-square', 780, 780, array('center', 'center'));
/**
 * WP Aston Manor Theme functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package WP Aston Manor Theme
 * @since WP Aston Manor Theme 0.1.0
 */
// Useful global constants
define('ASTON_MANOR_VERSION', '0.1.0');

/**
 * Add humans.txt to the <head> element.
 */
function pixelfire_header_meta() {
    $humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';

    echo apply_filters('pixelfire_humans', $humans);
}

add_action('wp_head', 'pixelfire_header_meta');

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since WP PixelFire Theme 1.0
 */
if (!isset($content_width))
    $content_width = 640; /* pixels */

if (!function_exists('astonmanor_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * @since WP Aston Manor Theme 1.0
     */
    function astonmanor_setup() {

        /**
         * Custom template tags for this theme.
         */
        require( get_template_directory() . '/inc/template-tags.php' );

        /**
         * Custom functions that act independently of the theme templates
         */
        require( get_template_directory() . '/inc/extras.php' );


        /**
         * Customizer additions
         */
        require( get_template_directory() . '/inc/customizer.php' );

        /**
         * WordPress.com-specific functions and definitions
         */
        //require( get_template_directory() . '/inc/wpcom.php' );

        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         */
        load_theme_textdomain('pixelfire', get_template_directory() . '/languages');

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support('automatic-feed-links');

        /**
         * Enable support for Post Thumbnails
         */
        add_theme_support('post-thumbnails');

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'pixelfire'),
            'mobile' => __('Mobile Menu', 'pixelfire') // secondary nav in footer
        ));


        /**
         * Add support for the Aside Post Formats
         */
        add_theme_support('post-formats', array('aside',));
    }

endif; // astonmanor_setup
add_action('after_setup_theme', 'astonmanor_setup');

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since WP PixelFire Theme 1.0
 */
function am_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'pixelfire'),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'id' => 'social-widget-header',
        'name' => __('Social Widget Header', 'pixelfire'),
        'description' => __('Used for adding Social Icons and links to right side of global header area', 'pixelfire'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'id' => 'footer-widget-1',
        'name' => __('Footer Widget Column 1', 'pixefire'),
        'description' => __('Used for footer widget area', 'pixefire'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'id' => 'footer-widget-2',
        'name' => __('Footer Widget Column 2', 'pixefire'),
        'description' => __('Used for footer widget area', 'pixefire'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'id' => 'footer-widget-3',
        'name' => __('Footer Widget Column 3', 'pixefire'),
        'description' => __('Used for footer widget area', 'pixefire'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'id' => 'footer-widget-4',
        'name' => __('Footer Widget Column 4', 'pixefire'),
        'description' => __('Used for footer widget area', 'pixefire'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));

    register_widget('pixelfire_social_widget');
    register_widget('sparkling_popular_posts');
    register_widget('sparkling_categories');
}

add_action('widgets_init', 'am_widgets_init');

/* --------------------------------------------------------------
  Theme Widgets
  -------------------------------------------------------------- */

require_once(get_template_directory() . '/inc/widgets/widget-categories.php');
require_once(get_template_directory() . '/inc/widgets/widget-social.php');
require_once(get_template_directory() . '/inc/widgets/widget-popular-posts.php');

/**
 * Enqueue scripts and styles
 */
function aston_manor_scripts() {
        /**
     * Check for Dev Environment if true load unminified scripts and css else load minified versions.
     */
    $minified = "";
    //echo $myDomain = $_SERVER['HTTP_HOST'];

    if (($_SERVER['HTTP_HOST'] === 'staging.astonmanornightclub.com') || $_SERVER['HTTP_HOST'] === 'astonmanornightclub.com' || $_SERVER['HTTP_HOST'] === 'aston-manor.dev') {
        $minified = ".min";
    } else {
        $minified = "";
    }
    wp_enqueue_style('style', get_stylesheet_uri());

    wp_enqueue_script('small-menu', get_template_directory_uri() . '/js/small-menu.js', array('jquery'), '20120206', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if (is_singular() && wp_attachment_is_image()) {
        wp_enqueue_script('keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array('jquery'), '20120202');
    }

    $postfix = ( defined('SCRIPT_DEBUG') && true === SCRIPT_DEBUG ) ? '' : '.min';
    //wp_enqueue_style( 'pixelfire-theme', get_template_directory_uri() . "/assets/css/wp_pixelfire_theme{$postfix}.css", array(), ASTON_MANOR_VERSION );
    wp_enqueue_style('pixelfire-css', get_template_directory_uri() . '/assets/css/theme-style'.$minified.'.css', array(), ASTON_MANOR_VERSION);
    wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/assets/css/font-awesome'.$minified.'.css', array(), ASTON_MANOR_VERSION);
    wp_enqueue_style('prettyPhoto-css', get_template_directory_uri() . '/assets/css/prettyPhoto'.$minified.'.css', array(), ASTON_MANOR_VERSION);
    wp_enqueue_script('pixelfire-js-min', get_template_directory_uri() . '/assets/js/wp_pixelfire_theme'.$minified.'.js', array('jquery'), ASTON_MANOR_VERSION, true);
    wp_enqueue_script('parallax-js', get_template_directory_uri() . '/assets/js/vendor/parallax'.$minified.'.js', array(), ASTON_MANOR_VERSION, true);
    wp_enqueue_script('jquery-vide-js', get_template_directory_uri() . '/assets/js/vendor/jquery.vide'.$minified.'.js', array(), ASTON_MANOR_VERSION, true);
    wp_enqueue_script('bxslider-js', get_template_directory_uri() . '/assets/js/vendor/jquery.bxslider'.$minified.'.js', array(), ASTON_MANOR_VERSION, true);
    wp_enqueue_script('prettyPhoto-js', get_template_directory_uri() . '/assets/js/vendor/jquery.prettyPhoto'.$minified.'.js', array(), ASTON_MANOR_VERSION, true);
}

add_action('wp_enqueue_scripts', 'aston_manor_scripts');

/**x
 * Implement the Custom Header feature
 */
require( get_template_directory() . '/inc/custom-header.php' );

/*
 * Loads the Options Panel
 *
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */

define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/admin/');
require_once dirname(__FILE__) . '/inc/admin/options-framework.php';
// Loads options.php from child or parent theme
$optionsfile = locate_template('options.php');
load_template($optionsfile);


add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

add_filter('pre_get_posts', 'query_post_type');

function query_post_type($query) {
    if (is_category() || is_tag()) {
        $post_type = get_query_var('post_type');
        if ($post_type)
            $post_type = $post_type;
        else
            $post_type = array('post', 'event-calendar'); // replace cpt to your custom post type
        $query->set('post_type', $post_type);
        return $query;
    }
}

// Add iimage class to WP placed images in post or pages
function image_tag_class($class) {
    $class .= ' img-responsive';
    return $class;
}

add_filter('get_image_tag_class', 'image_tag_class');

//function new_excerpt_more($more) {
//    global $post;
//    return '<br><a class="moretag" href="'. get_permalink($post->ID) . '">[Read More]</a>'; //Change to suit your needs
//}
// 
//add_filter( 'excerpt_more', 'new_excerpt_more' );

/* Modify the read more link on the_excerpt() */

function et_excerpt_length($length) {
    return 30;
}
add_filter('excerpt_length', 'et_excerpt_length');
 
/* Add a link  to the end of our excerpt contained in a div for styling purposes and to break to a new line on the page.*/
 
function et_excerpt_more($more) {
    global $post;
    return '<div class="view-full-post"><a href="'. get_permalink($post->ID) . '" class="view-full-post-btn">Read More</a></div>';
}
add_filter('excerpt_more', 'et_excerpt_more');

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');

function custom_widget_featured_image() {
  global $post;
  echo tribe_event_featured_image( $post->ID, 'instagram-square' );
}
add_action( 'tribe_events_list_widget_before_the_event_title', 'custom_widget_featured_image' );
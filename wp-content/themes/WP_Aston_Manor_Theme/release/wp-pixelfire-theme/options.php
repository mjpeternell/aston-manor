<?php

/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */
function optionsframework_option_name() {
    return 'pixelfire';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */
function optionsframework_options() {

    // Layout options
    //$site_layout = array('pull-left' => __('Right Sidebar', 'sparkling'),'pull-right' => __('Left Sidebar', 'sparkling'));
    // Test data
    $test_array = array(
        'one' => __('One', 'options_framework_theme'),
        'two' => __('Two', 'options_framework_theme'),
        'three' => __('Three', 'options_framework_theme'),
        'four' => __('Four', 'options_framework_theme'),
        'five' => __('Five', 'options_framework_theme')
    );

    // Multicheck Array
    $multicheck_array = array(
        'one' => __('French Toast', 'options_framework_theme'),
        'two' => __('Pancake', 'options_framework_theme'),
        'three' => __('Omelette', 'options_framework_theme'),
        'four' => __('Crepe', 'options_framework_theme'),
        'five' => __('Waffle', 'options_framework_theme')
    );

    // Multicheck Defaults
    $multicheck_defaults = array(
        'one' => '1',
        'five' => '1'
    );



    // Button Defaults
    $button_defaults = array(
        'size' => '16px',
        'face' => 'Arial',
        'radius' => '0',
        'style' => 'normal',
        'color' => '#000000'
    );

    // Button Options
    $button_options = array(
        //'sizes' => array('10','12','14','15','16'),
        'styles' => array('normal' => 'Normal', 'italic' => 'Italic', 'oblique' => 'Oblique'),
        'faces' => array(
            'arial' => 'Arial',
            'verdana' => 'Verdana, Geneva',
            'trebuchet' => 'Trebuchet',
            'georgia' => 'Georgia',
            'times' => 'Times New Roman',
            'tahoma' => 'Tahoma, Geneva',
            'roboto' => 'Roboto',
            'Roboto Condensed' => 'Roboto Condensed',
            'source-serif-pro' => 'Source Serif Pro',
            'Lato' => 'Lato',
            'Neuton' => 'Neuton',
            'Oswald' => 'Oswald',
            'Bitter' => 'Bitter',
            'Raleway' => 'Raleway',
            'palatino' => 'Palatino',
            'helvetica' => 'Helvetica',
            'Helvetica Neue' => 'Helvetica Neue'
        ),
        'weights' => array(
            'normal' => 'Normal',
            'bold' => 'Bold',
            'bolder' => 'Bolder',
            'lighter' => 'lighter',
            '100' => '100',
            '200' => '200',
            '300' => '300',
            '400' => '400',
            '500' => '500',
            '600' => '600',
            '700' => '700',
            '800' => '800',
            '900' => '900'
        ),
        'color' => true
    );

    // Typography Defaults
    $typography_defaults = array(
        'size' => '16px',
        'face' => 'Arial',
        'weight' => 'normal',
        'style' => 'normal',
        'color' => '#000000'
    );

    // Typography Options
    $typography_options = array(
        //'sizes' => array('10','12','14','15','16','18','20','22','24','26','28','30','32','34','36','42','48','60','61','62','63','64','65','66','67','68','69','90','90'),

        'faces' => array(
            'arial' => 'Arial',
            'verdana' => 'Verdana, Geneva',
            'trebuchet' => 'Trebuchet',
            'georgia' => 'Georgia',
            'times' => 'Times New Roman',
            'tahoma' => 'Tahoma, Geneva',
            'roboto' => 'Roboto',
            'Roboto Condensed' => 'Roboto Condensed',
            'source-serif-pro' => 'Source Serif Pro',
            'Lato' => 'Lato',
            'Oswald' => 'Oswald',
            'Bitter' => 'Bitter',
            'Raleway' => 'Raleway',
            'palatino' => 'Palatino',
            'helvetica' => 'Helvetica',
            'Helvetica Neue' => 'Helvetica Neue'
        ),
        'styles' => array('normal' => 'Normal', 'italic' => 'Italic', 'oblique' => 'Oblique'),
        'weights' => array(
            'normal' => 'Normal',
            'bold' => 'Bold',
            'bolder' => 'Bolder',
            'lighter' => 'lighter',
            '100' => '100',
            '200' => '200',
            '300' => '300',
            '400' => '400',
            '500' => '500',
            '600' => '600',
            '700' => '700',
            '800' => '800',
            '900' => '900'
        ),
        'color' => true
    );

    $radio = array('0' => __('No', 'pixelfire'), '1' => __('Yes', 'pixelfire'));

    // Pull all the categories into an array
    $options_categories = array();
    $options_categories_obj = get_categories();
    foreach ($options_categories_obj as $category) {
        $options_categories[$category->cat_ID] = $category->cat_name;
    }

    // Pull all tags into an array
    $options_tags = array();
    $options_tags_obj = get_tags();
    foreach ($options_tags_obj as $tag) {
        $options_tags[$tag->term_id] = $tag->name;
    }

    // Pull all the pages into an array
    $options_pages = array();
    $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
    $options_pages[''] = 'Select a page:';
    foreach ($options_pages_obj as $page) {
        $options_pages[$page->ID] = $page->post_title;
    }

    // If using image radio buttons, define a directory path
    $imagepath = get_template_directory_uri() . '/images/';


    // fixed or scroll position
    $fixed_scroll = array('scroll' => 'Scroll', 'fixed' => 'Fixed');

    $options = array();

    $options[] = array(
        'name' => __('Body', 'pixelfire'),
        'type' => 'heading'
    );
    $options[] = array(
        'name' => __('Body Background Image', 'pixelfire'),
        'desc' => __('Upload a 32px x 32px PNG/GIF image that will represent your websites favicon', 'pixelfire'),
        'id' => 'body_bg_image',
        'std' => '',
        'type' => 'background',
       
    );

    $options[] = array(
        'name' => __('Links', 'pixelfire'),
        'type' => 'heading'
    );

//	$options[] = array(
//		'name' => __('Do You want to display image slider on the Home Page?','pixelfire'),
//		'desc' => __('Check if you want to enable slider', 'pixelfire'),
//		'id'   => 'sparkling_slider_checkbox',
//		'std'  => 0,
//		'type' => 'checkbox'
//	);
//	$options[] = array(
//		'name'    => __('Slider Category', 'pixelfire'),
//		'desc'    => __('Select a category for the featured post slider', 'pixelfire'),
//		'id'      => 'sparkling_slide_categories',
//		'type'    => 'select',
//		'class'   => 'hidden',
//		'options' => $options_categories
//	);
//	$options[] = array(
//		'name'  => __('Number of slide items', 'pixelfire'),
//		'desc'  => __('Enter the number of slide items', 'pixelfire'),
//		'id'    => 'sparkling_slide_number',
//		'std'   => '3',
//		'class' => 'hidden',
//		'type'  => 'text'
//	);
//	$options[] = array(
//		'name'    => __('Website Layout Options', 'pixelfire'),
//		'desc'    => __('Choose between Left and Right sidebar options to be used as default', 'pixelfire'),
//		'id'      => 'site_layout',
//		'std'     => 'pull-left',
//		'type'    => 'select',
//		'class'   => 'mini',
//		'options' => $site_layout
//	);

    $options[] = array(
        'name' => __('Anchor Link Element color', 'pixelfire'),
        'desc' => __('Global link color. Default color used if no color is selected', 'pixelfire'),
        'id' => 'anchor_element_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Anchor Link Element color on hover', 'pixelfire'),
        'desc' => __('Global link hover color. Default used if no color is selected', 'pixelfire'),
        'id' => 'anchor_element_color_hover',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Favicons', 'pixelfire'),
        'type' => 'heading'
    );
    $options[] = array(
        'name' => __('Custom Favicon', 'pixelfire'),
        'desc' => __('Upload a 32px x 32px PNG/GIF image that will represent your websites favicon', 'pixelfire'),
        'id' => 'custom_favicon',
        'std' => '',
        'type' => 'upload'
    );

    $options[] = array(
        'name' => __('Buttons', 'pixelfire'),
        'type' => 'heading'
    );


//    $options[] = array(
//        'name' => __('Call For Action Text', 'pixelfire'),
//        'desc' => __('Enter the text for call for action section', 'pixelfire'),
//        'id' => 'w2f_cfa_text',
//        'std' => '',
//        'type' => 'textarea'
//    );
//    $options[] = array(
//        'name' => __('Call For Action Button Title', 'pixelfire'),
//        'desc' => __('Enter the title for Call For Action button', 'pixelfire'),
//        'id' => 'w2f_cfa_button',
//        'std' => '',
//        'type' => 'text'
//    );
//    $options[] = array(
//        'name' => __('CFA button link', 'pixelfire'),
//        'desc' => __('Enter the link for Call For Action button', 'pixelfire'),
//        'id' => 'w2f_cfa_link',
//        'std' => '',
//        'type' => 'text'
//    );
//    $options[] = array(
//        'name' => __('Call For Action Text Color', 'pixelfire'),
//        'desc' => __('Default used if no color is selected', 'pixelfire'),
//        'id' => 'cfa_color',
//        'std' => '',
//        'type' => 'color'
//    );
//    $options[] = array(
//        'name' => __('Call For Action Button Border Color', 'pixelfire'),
//        'desc' => __('Default used if no color is selected', 'pixelfire'),
//        'id' => 'cfa_btn_color',
//        'std' => '',
//        'type' => 'color'
//    );
//    $options[] = array(
//        'name' => __('Call For Action Button Text Color', 'pixelfire'),
//        'desc' => __('Default used if no color is selected', 'pixelfire'),
//        'id' => 'cfa_btn_txt_color',
//        'std' => '',
//        'type' => 'color'
//    );

    $options[] = array(
        'name' => __('Primary Button', 'pixelfire'),
        'desc' => __('Primary button adds styling to Call to Action button elements.', 'pixelfire'),
        'id' => 'button_styling',
        'std' => $button_defaults,
        'type' => 'button',
        'options' => $button_options
    );

    $options[] = array(
        'name' => __('Primary Button Background Color: Normal State', 'pixelfire'),
        'desc' => __('Default used if no color is selected', 'pixelfire'),
        'id' => 'primary_btn_bg_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Primary Button Background Color: Hover State ', 'pixelfire'),
        'desc' => __('Default used if no color is selected', 'pixelfire'),
        'id' => 'primary_btn_bg_color_hover',
        'std' => '',
        'type' => 'color'
    );


    $options[] = array(
        'name' => __('Secondary Button', 'pixelfire'),
        'desc' => __('Primary button adds styling to Call to Action button elements.', 'pixelfire'),
        'id' => 'secondary_button_styling',
        'std' => $button_defaults,
        'type' => 'button',
        'options' => $button_options
    );

    $options[] = array(
        'name' => __('Secondary Button Background Color: Normal State', 'pixelfire'),
        'desc' => __('Default used if no color is selected', 'pixelfire'),
        'id' => 'secondary_btn_bg_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Secondary Button Background Color: Hover State ', 'pixelfire'),
        'desc' => __('Default used if no color is selected', 'pixelfire'),
        'id' => 'secondary_btn_bg_color_hover',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Typography', 'pixelfire'),
        'type' => 'heading',
        'desc' => __('Select a font sizes, font family, font weight and font color', 'pixelfire'),
    );

    $options[] = array(
        'name' => __('Intstuctions:', 'pixelfire'),
        'desc' => __('Select a font sizes, font family, font weight and font color', 'pixelfire'),
    );

    $options[] = array(
        'name' => __('Main Body Text', 'pixelfire'),
        'desc' => __('Used in P tags', 'pixelfire'),
        'id' => 'main_body_typography',
        'std' => $typography_defaults,
        'type' => 'typography',
        'options' => $typography_options
    );

    $options[] = array(
        'name' => __('H1 Heading Text', 'pixelfire'),
        'desc' => __('Used in H1 tags', 'pixelfire'),
        'id' => 'h1_heading_typography',
        'std' => $typography_defaults,
        'type' => 'typography',
        'options' => $typography_options
    );

    $options[] = array(
        'name' => __('H2 Heading Text', 'pixelfire'),
        'desc' => __('Used in H2 tags', 'pixelfire'),
        'id' => 'h2_heading_typography',
        'std' => $typography_defaults,
        'type' => 'typography',
        'options' => $typography_options
    );

    $options[] = array(
        'name' => __('H3 Heading Text', 'pixelfire'),
        'desc' => __('Used in h3 tags', 'pixelfire'),
        'id' => 'h3_heading_typography',
        'std' => $typography_defaults,
        'type' => 'typography',
        'options' => $typography_options
    );

    $options[] = array(
        'name' => __('H4 Heading Text', 'pixelfire'),
        'desc' => __('Used in H4 tags', 'pixelfire'),
        'id' => 'h4_heading_typography',
        'std' => $typography_defaults,
        'type' => 'typography',
        'options' => $typography_options
    );

    $options[] = array(
        'name' => __('H5 Heading Text', 'pixelfire'),
        'desc' => __('Used in H5 tags', 'pixelfire'),
        'id' => 'h5_heading_typography',
        'std' => $typography_defaults,
        'type' => 'typography',
        'options' => $typography_options
    );

    $options[] = array(
        'name' => __('H6 Heading Text', 'pixelfire'),
        'desc' => __('Used in H6 tags', 'pixelfire'),
        'id' => 'h6_heading_typography',
        'std' => $typography_defaults,
        'type' => 'typography',
        'options' => $typography_options
    );

//    $options[] = array(
//        'name' => __('Heading Color', 'pixelfire'),
//        'desc' => __('Color for all headings (h1-h6)', 'pixelfire'),
//        'id' => 'heading_color',
//        'std' => '',
//        'type' => 'color'
//    );

    $options[] = array(
        'name' => __('Link Color', 'pixelfire'),
        'desc' => __('Default used if no color is selected', 'pixelfire'),
        'id' => 'link_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Link:hover Color', 'pixelfire'),
        'desc' => __('Default used if no color is selected', 'pixelfire'),
        'id' => 'link_hover_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Header', 'pixelfire'),
        'type' => 'heading'
    );

    $options[] = array(
        'name' => __('Top nav background color', 'pixelfire'),
        'desc' => __('Default used if no color is selected', 'pixelfire'),
        'id' => 'nav_bg_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Top nav item color', 'pixelfire'),
        'desc' => __('Link color', 'pixelfire'),
        'id' => 'nav_link_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Top nav item hover color', 'pixelfire'),
        'desc' => __('Link:hover color', 'pixelfire'),
        'id' => 'nav_item_hover_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Top nav dropdown background color', 'pixelfire'),
        'desc' => __('Background of dropdown item hover color', 'pixelfire'),
        'id' => 'nav_dropdown_bg',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Top nav dropdown item color', 'pixelfire'),
        'desc' => __('Dropdown item color', 'pixelfire'),
        'id' => 'nav_dropdown_item',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Top nav dropdown item hover color', 'pixelfire'),
        'desc' => __('Dropdown item hover color', 'pixelfire'),
        'id' => 'nav_dropdown_item_hover',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Top nav dropdown item background hover color', 'pixelfire'),
        'desc' => __('Background of dropdown item hover color', 'pixelfire'),
        'id' => 'nav_dropdown_bg_hover',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer', 'pixelfire'),
        'type' => 'heading'
    );

    $options[] = array(
        'name' => __('Footer background color', 'pixelfire'),
        'id' => 'footer_bg_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer widget area background color', 'pixelfire'),
        'id' => 'footer_widget_bg_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer Background Image', 'pixelfire'),
        'desc' => __('Upload a 32px x 32px PNG/GIF image that will represent your websites favicon', 'pixelfire'),
        'id' => 'footer_bg_image',
        'std' => '',
        'type' => 'upload'
    );

    $options[] = array(
        'name' => __('Footer text color', 'pixelfire'),
        'id' => 'footer_text_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer link color', 'pixelfire'),
        'id' => 'footer_link_color',
        'std' => '',
        'type' => 'color'
    );
    $options[] = array(
        'name' => __('Footer Link Hover color', 'pixelfire'),
        'id' => 'footer_link_hover_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer information', 'pixelfire'),
        'desc' => __('Copyright text in footer', 'pixelfire'),
        'id' => 'custom_footer_text',
        'std' => '<a href="' . esc_url(home_url('/')) . '" title="' . esc_attr(get_bloginfo('name', 'display')) . '" >' . get_bloginfo('name', 'display') . '</a>  All rights reserved.',
        'type' => 'textarea'
    );

    $options[] = array(
        'name' => __('Social Icon and Link Settings', 'pixelfire'),
        'type' => 'heading'
    );

    $options[] = array(
        'name' => __('Header - Social Icon Background Color', 'pixelfire'),
        'desc' => __('Default color is used if no color is selected.', 'pixelfire'),
        'id' => 'social_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Header - Social Icon Link Color', 'pixelfire'),
        'desc' => __('Default link color is used if no color is selected.', 'pixelfire'),
        'id' => 'social_link_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Header - Social Icon Link Hover Color', 'pixelfire'),
        'desc' => __('Default link hover color is used if no color is selected.', 'pixelfire'),
        'id' => 'social_link_hover_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer - Social Icon Link Color', 'pixelfire'),
        'desc' => __('Default link color is used if no color is selected.', 'pixelfire'),
        'id' => 'footer_social_link_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer -  Social Icon Link Hover Color', 'pixelfire'),
        'desc' => __('Default link hover color is used if no color is selected.', 'pixelfire'),
        'id' => 'footer_social_link_hover_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Footer - Social Icon Background Color', 'pixelfire'),
        'desc' => __('Default color is used if no color is selected.', 'pixelfire'),
        'id' => 'social_footer_color',
        'std' => '',
        'type' => 'color'
    );

    $options[] = array(
        'name' => __('Add full URL for your social network profiles', 'pixelfire'),
        'desc' => __('Facebook', 'pixelfire'),
        'id' => 'social_facebook',
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_twitter',
        'desc' => __('Twitter', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_googleplus',
        'desc' => __('Google+', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_youtube',
        'desc' => __('YouTube', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_vimeo',
        'desc' => __('Vimeo', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_linkedin',
        'desc' => __('LinkedIn', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_pinterest',
        'desc' => __('Pinterest', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_rss',
        'desc' => __('RSS Feed', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_tumblr',
        'desc' => __('Tumblr', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_flickr',
        'desc' => __('Flickr', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_instagram',
        'desc' => __('Instagram', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_dribbble',
        'desc' => __('Dribbble', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_skype',
        'desc' => __('Skype', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_foursquare',
        'desc' => __('Foursquare', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_soundcloud',
        'desc' => __('SoundCloud', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'id' => 'social_github',
        'desc' => __('GitHub', 'pixelfire'),
        'std' => '',
        'class' => 'mini',
        'type' => 'text'
    );

    $options[] = array(
        'name' => __('Other', 'pixelfire'),
        'type' => 'heading'
    );

    $options[] = array(
        'name' => __('Custom CSS', 'pixelfire'),
        'desc' => __('Additional CSS', 'pixelfire'),
        'id' => 'custom_css',
        'std' => '',
        'type' => 'textarea'
    );

    return $options;
}

<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>

<section id="primary" class="content-area">
    <div id="content" class="site-content" role="main">

        <?php if (have_posts()) : ?>
            <div class="wysiwyg-row">
                <header class="page-header">
                    <h1 class="page-title"><?php printf(__('Search Results for: %s', 'pixelfire'), '<span>' . get_search_query() . '</span>'); ?></h1>
                </header><!-- .page-header -->
                <div class="col-lg-8">  
                    <?php //pixelfire_content_nav('nav-above'); ?>

                    <?php /* Start the Loop */ ?>
                    <?php while (have_posts()) : the_post(); ?>

                        <?php get_template_part('content', 'search'); ?>

                    <?php endwhile; ?>

                    <?php pixelfire_content_nav('nav-below'); ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        <?php else : ?>
            <div class="wysiwyg-row">
                <div class="col-lg-8">  
                    <?php get_template_part('no-results', 'search'); ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        <?php endif; ?>

    </div><!-- #content .site-content -->
</section><!-- #primary .content-area -->


<?php get_footer(); ?>
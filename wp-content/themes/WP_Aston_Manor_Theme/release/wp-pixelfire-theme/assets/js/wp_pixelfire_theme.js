/*! WP PixelFire Theme - v0.1.0 - 2016-11-18
 * pixelfire.net
 * Copyright (c) 2016; * Licensed GPLv2+ */
function listenWidth(e) {
    if (jQuery(window).width() <= 980) {
        jQuery("#ab-left").remove().insertAfter(jQuery("#ab-right"));
        //jQuery("#gallery-vid-left").remove().insertAfter(jQuery("#gallery-vid-right")); 
    }
    if (jQuery(window).width() >= 981) {
        jQuery("#ab-right").remove().insertAfter(jQuery("#ab-left"));
        //jQuery("#gallery-vid-right").remove().insertAfter(jQuery("#gallery-vid-left"));
    }
}

jQuery(document).ready(function () {

//    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
//        //custom_markup: '<div class="dark_square"></div>'
//    });

    jQuery('.bxslider').bxSlider({
        infiniteLoop: false,
        hideControlOnEnd: true,
        slideWidth: 480,
        prevText: '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
        nextText: '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
    });


    function sliderResize() {
        //My Window Height detections
        var myWindow = jQuery(window).innerWidth();
        console.log(myWindow);
        var tileSize = "";
        if (myWindow <= 600) {
            tileSize = myWindow;
            jQuery('.bxslider .slide').css({width: tileSize});
            jQuery('.bx-viewport').css({height: tileSize, 'width': myWindow});
            jQuery('.bx-wrapper').css({
                "width": "100%",
                "max-width": "100%"
            });
        } else if (myWindow <= 768) {
            tileSize = myWindow / 2;
            jQuery('.bxslider .slide').css({width: tileSize});
            jQuery('.bx-viewport').css({height: tileSize, 'width': myWindow});
            jQuery('.bx-wrapper').css({
                "width": "100%",
                "max-width": "100%"
            });
        } else if (myWindow <= 1024) {
            tileSize = myWindow / 3;
            jQuery('.bxslider .slide').css({
                "width": tileSize
            });
            jQuery('.bx-viewport').css({height: tileSize, 'width': myWindow});
            jQuery('.bx-wrapper').css({
                "width": "100%",
                "max-width": "100%"
            });

        } else {
            tileSize = myWindow / 4;
            jQuery('.bxslider .slide').css({
                "width": tileSize
            });
            jQuery('.bx-viewport').css({height: tileSize, 'width': myWindow});
            jQuery('.bx-wrapper').css({
                "width": "100%",
                "max-width": "100%"
            });
        }
    }

    jQuery(window).resize(function () {
        sliderResize();
        listenWidth();
    });

    jQuery(window).load(function () {
        sliderResize();
        listenWidth();
    });

// Listen for orientation changes
    window.addEventListener("orientationchange", function () {
        sliderResize();
        listenWidth();
    }, false);

//scroll to top functionality
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-to-top').fadeIn();
        } else {
            jQuery('.scroll-to-top').fadeOut();
        }
    });

//Click event to scroll to top
    jQuery('.scroll-to-top').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    jQuery('.accordion-tabs-minimal').each(function (index) {
        jQuery(this).children('li').first().children('a').addClass('is-active').next().addClass('is-open').show();
    });
    jQuery('.accordion-tabs-minimal').on('click', 'li > a.tab-link', function (event) {
        if (!jQuery(this).hasClass('is-active')) {
            event.preventDefault();
            var accordionTabs = jQuery(this).closest('.accordion-tabs-minimal');
            accordionTabs.find('.is-open').removeClass('is-open').hide();

            jQuery(this).next().toggleClass('is-open').toggle();
            accordionTabs.find('.is-active').removeClass('is-active');
            jQuery(this).addClass('is-active');
        } else {
            event.preventDefault();
        }
    });



    var menuToggle = jQuery("#js-mobile-menu").unbind();
    jQuery("#js-navigation-menu").removeClass("show");

    menuToggle.on("click", function (e) {
        e.preventDefault();

        jQuery(".navigation-tools").addClass("show-all");
        jQuery("#js-navigation-menu").slideToggle(300, "linear", function () {
            jQuery("#js-navigation-menu").addClass("show");
            if (jQuery("#js-navigation-menu").is(":hidden")) {
                jQuery("#js-navigation-menu").removeClass("show");
                jQuery("#js-navigation-menu").removeAttr("style");
                jQuery(".navigation-tools").removeClass("show-all");

            }
        });
    });
});

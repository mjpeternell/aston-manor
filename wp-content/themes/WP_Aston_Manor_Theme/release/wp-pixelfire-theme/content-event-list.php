<?php
/**
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('archive-list-item'); ?>>


    <?php if (is_search()) : // Only display Excerpts for Search ?>
        <div class="entry-summary">
            <?php the_excerpt(); ?>
        </div><!-- .entry-summary -->
    <?php else : ?>
        <div class="entry-content row">
            <div class="col-lg-4">
                <div class="image_holder">

                    <?php if (has_post_thumbnail()) : ?>
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'pixelfire'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_post_thumbnail(); ?></a>
                    <?php else : ?>
                        <img src="/wp-content/themes/WP_Aston_Manor_Theme/assets/images/059-default-archive-image-780x780.jpg" class="img-responsive" alt="Archive Image" />
                    <?php endif; ?>

                </div>
            </div>
            <div class="col-lg-8">
                <div class="event-form-links">
                    <?php if (get_field('guest_list_link')): ?>
                        <a href="javascript:void(0)" data-optin-slug="<?php the_field('guest_list_link'); ?>" class="link guest black manual-optin-trigger" role="button">Guest List</a>
                    <?php else: ?>
                        <a href="javascript:void(0)" class="link guest black disabled" role="button">Guest List</a>
                    <?php endif; ?>
                        
                    <?php if (get_field('reservations_link')): ?>
                       <a href="javascript:void(0)" data-optin-slug="<?php the_field('reservations_link'); ?>" class="link reserve white manual-optin-trigger" role="button">Reserve a Booth</a>
                     <?php else: ?>
                       <a href="javascript:void(0)" class="link reserve white disabled" role="button">Reserve a Booth</a>
                    <?php endif; ?>
                </div>
                <header class="entry-header">
                    <h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'pixelfire'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?> </a></h1>
                    <span class="meta">
                        <?php
                        global $wp_query;
                        $postid = $wp_query->post->ID;
                        if (get_post_meta($postid, 'event_date', true)) {
                            $date = get_field('event_date', false, false);
                            $date = new DateTime($date);
                            echo '<span><i class="fa fa-clock-o" aria-hidden="true"></i> ' . $date->format('M j, Y') . '</span>';
                        }
                        if (get_post_meta($postid, 'facebook_event', true)) {
                            $fb_url = get_field('facebook_event', false, false);
                            echo '<span><i class="fa fa-facebook-official" aria-hidden="true"></i> <a href="" target="_blank">RSVP</a></span>';
                        }
                        ?></span>
                    <?php if ('post' == get_post_type()) : ?>
                        <div class="entry-meta">
                            <?php pixelfire_posted_on(); ?>
                        </div><!-- .entry-meta -->
                    <?php endif; ?>
                </header><!-- .entry-header -->
                <div class="content">
                    <?php the_excerpt(); ?>
                    <?php //the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'pixelfire' ) ); ?>
                    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?> 
                </div>

                <footer class="entry-meta archive-footer">
                    <?php if ('post' == get_post_type()) : // Hide category and tag text for pages on Search   ?>
                        <?php
                        /* translators: used between list items, there is a space after the comma */
                        $categories_list = get_the_category_list(__(', ', 'pixelfire'));
                        if ($categories_list && pixelfire_categorized_blog()) :
                            ?>
                            <span class="cat-links">
                                <?php printf(__('Posted in %1$s', 'pixelfire'), $categories_list); ?>
                            </span>
                        <?php endif; // End if categories   ?>

                        <?php
                        /* translators: used between list items, there is a space after the comma */
                        $tags_list = get_the_tag_list('', __(', ', 'pixelfire'));
                        if ($tags_list) :
                            ?>
                            <span class="sep"> | </span>
                            <span class="tags-links">
                                <?php printf(__('Tagged %1$s', 'pixelfire'), $tags_list); ?>
                            </span>
                        <?php endif; // End if $tags_list   ?>
                    <?php endif; // End if 'post' == get_post_type() ?>

                    <?php if (!post_password_required() && ( comments_open() || '0' != get_comments_number() )) : ?>
                        <span class="sep"> | </span>
                        <span class="comments-link"><?php comments_popup_link(__('Leave a comment', 'pixelfire'), __('1 Comment', 'pixelfire'), __('% Comments', 'pixelfire')); ?></span>
                    <?php endif; ?>

                    <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="sep"> | </span><span class="edit-link">', '</span>'); ?>
                </footer><!-- .entry-meta -->
            </div>
        </div><!-- .entry-content -->
    <?php endif; ?>


</article><!-- #post-<?php the_ID(); ?> -->

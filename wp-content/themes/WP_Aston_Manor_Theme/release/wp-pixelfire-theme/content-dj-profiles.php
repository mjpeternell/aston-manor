<?php
/**
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row">
        <header class="col-lg-12 entry-header">
            <h2 class="entry-title"><?php the_title(); ?></h2>
        </header><!-- .entry-header -->
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?php the_post_thumbnail(); ?>
        </div>

        <div class="col-lg-6">
            <div class="entry-content">
                <?php the_content(); ?>
                <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
            </div><!-- .entry-content -->
            <div class="event-details">
                <ul>
                    <?php
                    if (get_field('artist_site')) {
                        echo '<li><i class="fa fa-music" aria-hidden="true"></i><a href="' . get_field('artist_site') . '" target="_blank"><span class="acf-field">Artist Website</span></a></li>';
                    }
                    if (get_field('sound_cloud_site')) {
                        echo '<li><i class="fa fa-soundcloud" aria-hidden="true"></i><a href="' . get_field('sound_cloud_site') . '" target="_blank"><span class="acf-field">Listen</span></a></li>';
                    }


                    ?>
                </ul>
            </div>     
            <footer class="entry-meta">
                <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
            </footer><!-- .entry-meta -->
        </div>        
    </div>
</article><!-- #post-<?php the_ID(); ?> -->

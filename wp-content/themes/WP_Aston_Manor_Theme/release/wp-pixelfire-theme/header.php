<?php
/**
 * The template for displaying the header.
 *
 * @package WP PixelFire Theme
 * @since 0.1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php if (is_singular() && pings_open(get_queried_object())) : ?>
            <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php endif; ?>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <header class="navigation" role="banner">
            <div class="navigation-wrapper">

                <?php
                $header_image = get_header_image();
                if (!empty($header_image)) {
                    ?>
                    <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home" class="logo">
                        <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" />
                    </a>
                <?php } ?>

                <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu">MENU</a>
                <?php pixelfire_header_menu(); ?>
                <div class="navigation-tools">
                    <div class="social">
                        <?php if (is_active_sidebar('social-widget-header')) : ?>
                                <?php dynamic_sidebar('social-widget-header'); ?>
                           <!-- .widget-area .first -->
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </header>
        <div id="page" class="hfeed site">
            <div id="main" class="site-main">
<?php
/**
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row">
        <header class="col-lg-12 entry-header">
            <h2 class="entry-title"><?php the_title(); ?></h2>
        </header><!-- .entry-header -->
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?php the_post_thumbnail(); ?>
        </div>

        <div class="col-lg-6">
            <div class="entry-content">
                <?php the_content(); ?>
                <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
            </div><!-- .entry-content -->
            <div class="event-details">
<!--                <h3>Event Details</h3>-->
                <ul>
                    <?php
                    if (get_field('event_date')) {
                        echo '<li><i class="fa fa-calendar-o" aria-hidden="true"></i>Doors:<span class="acf-field">' . get_field('event_date') . '</span></li>';
                    }
                    if (get_field('doors')) {
                        echo '<li><i class="fa fa-calendar-o" aria-hidden="true"></i>Doors:<span class="acf-field">' . get_field('doors') . '</span></li>';
                    }
                    if (get_field('artist_name')) {
                        //echo '<li><i class="fa fa-link" aria-hidden="true"></i>Artists:<span class="acf-field">' . get_field('artists') . '</span></li>';
                        if (get_field('artist_page')) {
                        echo '<li><i class="fa fa-music" aria-hidden="true"></i>Artists: <a href="' . get_field('artist_page') . '"><span class="acf-field">' . get_field('artist_name') . '</span></a></li>';
                    }
                    }
                    
                    if (get_field('type')) {
                        echo '<li><i class="fa fa-link" aria-hidden="true"></i>Type:<span class="acf-field">' . get_field('type') . '</span></li>';
                    }
                    if (get_field('facebook_event')) {
                        echo '<li><i class="fa fa-facebook-official" aria-hidden="true"></i><a href="' . get_field('facebook_event') . '" target="_blank">Facebook Event</a></li>';
                    }
                    if (get_field('gallery')) {
                        echo '<li><i class="fa fa-picture-o" aria-hidden="true"></i><a href="' . get_field('gallery') . '" target="_blank">Gallery Link</a></li>';
                    }
                   
                   
                    if (get_field('artist_site')) {
                        echo '<li><i class="fa fa-music" aria-hidden="true"></i>Artist Website: <a href="' . get_field('artist_site') . '" target="_blank"><span class="acf-field">Artist Site</span></a></li>';
                    }
                    if (get_field('sound_cloud_site')) {
                        echo '<li><i class="fa fa-soundcloud" aria-hidden="true"></i>Sound Cloud:<a href="' . get_field('sound_cloud_site') . '" target="_blank"><span class="acf-field">Visit</span></a></li>';
                    }


                    ?>
                </ul>
            </div>     
            <footer class="entry-meta">
                <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
            </footer><!-- .entry-meta -->
        </div>        
    </div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php

/**
 * Social  Widget
 * Sparkling Theme
 */
class pixelfire_social_widget extends WP_Widget
{
    function pixelfire_social_widget() {

        $widget_ops = array(
            'classname' => 'social-widget',
            'description' => __( "Social Icon and Link Widget. Uses Font-Awesome Icons." ,'pixelfire')
        );
	$this->WP_Widget('sparkling-social', __('Social Icons and Link Widget','pixelfire'), $widget_ops);
    }

    function widget($args , $instance) {
    	extract($args);
        $title = ($instance['title']) ? $instance['title'] : __('Follow us' , 'pixelfire');

      echo $before_widget;
      //echo $before_title;
      //echo $title;
      //echo $after_title;

		/**
		 * Widget Content
		 */
    ?>

    <!-- social icons -->
    <div class="social-icons" role="complementary">
    <?php sparkling_social(); ?>
    </div><!-- end social icons -->
    <?php 
    echo $after_widget;
    }
    function form($instance) {
        if(!isset($instance['title'])) $instance['title'] = __('Follow us' , 'pixelfire');
    ?>
    <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title ','pixelfire') ?></label>
    <input type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo $this->get_field_name('title'); ?>" id="<?php $this->get_field_id('title'); ?>" class="widefat" />
    </p>

    <?php
    }

}

?>
<section id="Calander" class="section calendar non-parallax-window">
    <header class="section-header text-center">
        <h2>Coming Soon to Aston</h2>
        <h3>Check Out Some of Our Upcoming Events</h3>
    </header>
    <?php the_content(); ?>    
    <div class="bxslider">
        <?php
        $argsx = array(
            'post_type' => 'event-calendar',
            'posts_per_page' => 12,
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'ASC',
        );
        $future_posts = get_posts($argsx);
        foreach ($future_posts as $kk) {
            $thumb_size = 'instagram-square';
            $img_id = get_post_thumbnail_id($kk->ID); // This gets just the ID of the img
            $image = wp_get_attachment_image_src($img_id, $thumb_size); // Get URL of the image, and size can be set here too (same as with get_the_post_thumbnail, I think)
            $alt_text = get_post_meta($img_id, '_wp_attachment_image_alt', true);
            $perm = get_permalink($kk->ID);
            ?>
            <div class="slide">
                <div class="image_holder">
                    <span class="image">
                        <span class="image_pixel_hover"></span>
                        <a  target="_self" href="<?php echo $perm; ?>"><img src="<?php echo $image[0]; ?>" class="slide-image" alt="<?php echo $alt_text; ?>" /></a>
                    </span>
                    <div class="hover_feature_holder">
                        <div class="hover_feature_holder_outer">
                            <div class="hover_feature_holder_inner">
                                <h3 class="portfolio_title"><?php echo $kk->post_title; ?></h3>
                                <span class="separator small"></span>

                                <div class="project_category">
                                    <div class="tags">
                                        <?php
                                        foreach ((get_the_category($kk->ID)) as $category) {
                                            echo "<span>" . $category->cat_name . "</span>";
                                            //print_r($category);
                                        }
                                        ?>
                                    </div>
                                    <div class="tags">
                                        <?php
                                        $posttags = get_the_tags($kk->ID);
                                        if ($posttags) {
                                            foreach ($posttags as $tag) {
                                                echo "<span>" . $tag->name . "</span>";
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <a class="lightbox qbutton white small" href="<?php echo $image[0]; ?>" title="<?php echo $alt_text; ?>" rel="prettyPhoto">zoom</a>
                                <a class="qbutton white small" target="_self" href="<?php echo $perm; ?>">view</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        <?php } ?>
        <?php wp_reset_query(); ?>
    </div>
</section>
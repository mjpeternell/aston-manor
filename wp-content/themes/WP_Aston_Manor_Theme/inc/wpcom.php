<?php
/**
 * WordPress.com-specific functions and definitions
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */

global $themecolors;

/**
 * Set a default theme color array for WP.com.
 *
 * @global array $themecolors
 * @since WP PixelFire Theme 1.0
 */
$themecolors = array(
	'bg' => '',
	'border' => '',
	'text' => '',
	'link' => '',
	'url' => '',
);
